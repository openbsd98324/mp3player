


// apt-get install -y      libao-dev  libmpg123-dev  
// cc -O2 -o mp3player mp3player.c -lmpg123 -lao


//          libmp123 
// apt-get install -y      libao-dev  libmpg123-dev  
// cc -O2 -o mp3player mp3player.c -lmpg123 -lao
//npkg( "   libao-dev  libmpg123-dev   " ); 



// apt-get install -y      libao-dev  libmpg123-dev  
// cc -O2 -o mp3player mp3player.c -lmpg123 -lao
// Thanks J. Huang.


// It’s a little complicated to play HTTP URLs because you can’t pass a URL directly to mpg123_open. We should use mpg123_open_feed instead. We also use libcurl to read HTTP URLs and feed the received data for decoding using mpg123_feed. Then we use mpg123_decode_frame to try to decode a audio frame. Based on the return value of mpg123_decode_frame, we are able to decide the following situations: a frame is successfuly decoded, a new decoding format is encountered or more data is required.



#include <curl/curl.h>
#include <mpg123.h>
#include <ao/ao.h>

#define BITS 8

mpg123_handle *mh = NULL;
ao_device *dev = NULL;

size_t play_stream(void *buffer, size_t size, size_t nmemb, void *userp)
{
    int err;
    off_t frame_offset;
    unsigned char *audio;
    size_t done;
    ao_sample_format format;
    int channels, encoding;
    long rate;

    mpg123_feed(mh, (const unsigned char*) buffer, size * nmemb);
    do {
        err = mpg123_decode_frame(mh, &frame_offset, &audio, &done);
        switch(err) {
            case MPG123_NEW_FORMAT:
                mpg123_getformat(mh, &rate, &channels, &encoding);
                format.bits = mpg123_encsize(encoding) * BITS;
                format.rate = rate;
                format.channels = channels;
                format.byte_format = AO_FMT_NATIVE;
                format.matrix = 0;
                dev = ao_open_live(ao_default_driver_id(), &format, NULL);
                break;
            case MPG123_OK:
                ao_play(dev, audio, done);
                break;
            case MPG123_NEED_MORE:
                break;
            default:
                break;
        }
    } while(done > 0);

    return size * nmemb;
}




int main(int argc, char *argv[])
{
    if(argc < 2)
        return 0;

    ao_initialize();
    
    mpg123_init();
    mh = mpg123_new(NULL, NULL);
    mpg123_open_feed(mh);

    CURL *curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, play_stream);
    curl_easy_setopt(curl, CURLOPT_URL, argv[1]);
    curl_easy_perform(curl);
    curl_easy_cleanup(curl);

    mpg123_close(mh);
    mpg123_delete(mh);
    mpg123_exit();

    ao_close(dev);
    ao_shutdown();

    return 0;
}

/*
Save the code as playurl.c and build it. Remember you must first install libmpg123 libao and libcurl.

gcc -O2 -o playurl playurl.c -lmpg123 -lao -lcurl
Run the programm with a mp3 url in command line.

./playurl http://url.to/file.mp3
*/


